package service;

import domain.Order;
import domain.Product;
import repository.Repo1;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

// This class is the service that takes care of the order operations.
public class Service {
    private Repo1 r1;
    public Service(Repo1 r1) {
        this.r1 = r1;
    }
    public Order make(List<Product> l) {
        Order o = new Order(Date.from(Instant.now()), 2L);
        o.setProducts(l);
        o.setB();

        r1.saveOrder(o);

        return o;
    }
}
