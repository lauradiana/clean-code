package repository;

import domain.Order;
import domain.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Repo1 {
    Map<Long, Order> list;
    Repo2 repo2;
    public Repo1(Repo2 r2) { list=new HashMap<>(); repo2 = r2;}
    public Order get(Long id){
        return list.get(id);
    }
    public Iterable<Order> all() {
        return list.values();
    }
    public Order saveOrder(Order o) {
        // saves the order to the list and returns null.
        list.put(o.getId(),o);
        return null;
    }
    // This function adds a discount to all the products
    public Order modify(Order o, Integer d) {
        repo2.update(d);
        o.setB();

        return o;

    }




}
