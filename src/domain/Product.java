package domain;

public class Product{
    private Long id;
    private String n;
    private String d;
    private Long p;
    private String manufacturer;

    public Product(Long id, String n, String description, Long p) {
        this.id = id;
        this.n = n;
        this.d = description;
        this.p = p;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




    public String getN() {
        return n;
    }

    public String getD() {
        return d;
    }

    public void setP(Long p) {
        this.p = p;
    }

    public Long getP(){
        return p;
    }
}
