package domain;

import java.util.Date;
import java.util.List;

public class Order{
    private Date a; //the date of the order
    private String d; // the description of the order

    private Long b; //the total sum of the order
    private Long id; //the id of the user that made the order


    private List<Product> products;



    public Order(Date a,Long id){
        this.a=a;
        this.id=id;
    }

//    public void setA(Date a) {
//        this.a = a;
//    }
//
//    public void setD(String d) {
//        this.d = d;
//    }

    public Long getId() {
        return id;
    }

    public void setProducts(List<Product> l){
        this.products = l;
    }

    public void setB() {
        Long s = 0L;
        //calculate the sum of all products
        for(Product p: this.products) {
            s += p.getP();
        }
        //Sets this sum to the sum of the order.
        this.b = s;
        //Sets the description of the order.
        this.d = "Final order" + this.id.toString();
    }

    public void print(){
        System.out.println(id + " " + d + " " + b);
    }
}
