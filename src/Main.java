import domain.Order;
import domain.Product;
import repository.Repo1;
import repository.Repo2;
import service.Service;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String args[]){
        Repo2 r = new Repo2();
        Repo1 r2 = new Repo1(r);
        Service s = new Service(r2);
        Product p1 = new Product(1L, "name1", "description1", 30L);
        Product p2 = new Product(2L, "name2", "description2", 50L);
        r.save(p1);
        r.save(p2);
        List<Product> l = new ArrayList<>();
        l.add(p1);
        l.add(p2);
        Order o = s.make(l);
        o.print();

        o = r2.modify(o, 30);
        o.print();
    }
}
